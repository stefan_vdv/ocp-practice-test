public class CashRegister implements AutoCloseable {
    private final String code = "101";
    private Boolean open;

    public CashRegister() {
        open = false;
    }

    public void open(String code) {
        if (this.code.equals(code)) {
            this.open = true;
            System.out.println("Cash register opened");
        }
    }

    @Override
    public void close() throws Exception {
        if (open) {
            open = false;
        } else {
            throw new Exception("Register not opened");
        }
    }
}
