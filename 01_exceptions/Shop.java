import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Shop {
    // TODO: Oefeningen om te bekijken: Oef 37 (chapter 4)

    static List<String> originalMenu = Arrays.asList("Mexicano", "Ham & kaas", "Ham", "Kaas", "Gezond", "Spek & ei");
    static List<String> sandWiches = new ArrayList<>(originalMenu);

    public static void main(String[] args) {
        Shop shop = new Shop();

        System.out.println("What would you like to do: \n 1. Order \n 2. Add to list \n 3. Edit list \n 4. Remove from list");

        try (Scanner in = new Scanner(System.in)) {

            String choice = shop.readInput(in);

            switch (choice){
                case "1":
                    shop.orderSandwich(in);
                    break;
                case "2":
                    shop.addToList(in);
                    sandWiches.forEach(System.out::println);
                    break;
            }

            System.out.println("Enter cashRegister code");
            shop.openRegister(in.nextLine());
        }
    }

    private String readInput(Scanner in) {
        String choice = null;
        try {
            choice = in.nextLine();
            if (choice.isBlank()) {
                throw new IOException("order is empty");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return choice;
    }

    private void openRegister(String code) {
        try (CashRegister cashRegister = new CashRegister()) {
            cashRegister.open(code);
        } catch (Exception e ) {
            System.err.println(e.getMessage());
        }
    }

    private void orderSandwich(Scanner in){
        System.out.println("What sandwich would you like to order: ");

        sandWiches.stream()
                    .sorted()
                    .forEach(System.out::println);

        String sandwich = readInput(in);

        boolean correctOrder = sandWiches.stream()
                        .anyMatch( x -> x.equalsIgnoreCase(sandwich));

        if(correctOrder) {
            System.out.println("Order: " + sandwich);
        } else {
            System.out.println("We don't have this sandwich");
        }


    }

    private void addToList(Scanner in ){
        System.out.println("What sandwich would you like to add");
        String newSandwich = readInput(in);

        boolean alreadyExists = sandWiches.stream().anyMatch(x -> x.equalsIgnoreCase(newSandwich));
        if (alreadyExists){
            System.out.println("We already have this sandwich");
        } else {
            sandWiches.add(newSandwich);
            System.out.println("Sandwich added to our menu!");
        }
    }
}
