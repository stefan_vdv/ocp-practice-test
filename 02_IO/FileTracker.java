import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileTracker {

    public static void main(String[] args) {
        Path rootPath = Path.of("").toAbsolutePath();
        System.out.println("Path is : " + rootPath);

        try {
            Path path = rootPath.resolve("resources/nonExistent/..").normalize().toRealPath();
            System.out.println(path);

            System.out.println("Amount of files: " + countFiles(path));
            System.out.println("Amount of directories: " + countDirectories(path));

            readFile(path.resolve("layer1"));

            Files.copy(path.resolve("layer1"), path.resolve("deeper/againDeeper/deepest/layer1"));
            System.out.println("Amount of files after copy: " + countFiles(path));
            readFile(path.resolve("deeper/againDeeper/deepest/layer1"));
            
            Files.delete(path.resolve("deeper/againDeeper/deepest/layer1"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Long countFiles(Path path) throws IOException {
        return Files.walk(path)
                .filter(Files::isRegularFile)
                .count();
    }

    public static Long countDirectories(Path path) throws IOException {
        return Files.walk(path)
                .filter(Files::isDirectory)
                .count();
    }

    public static void readFile(Path path) throws IOException {
        try(var reader = Files.newBufferedReader(path)){
            reader.lines()
                    .forEach(System.out::println);
        }
    }
}
