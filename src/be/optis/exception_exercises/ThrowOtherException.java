package be.optis.exception_exercises;
import java.io.IOException;
class Organ {
    public void operate() throws IOException {
        throw new RuntimeException("Not supported");
    }
}
public class ThrowOtherException extends Organ {

        public void operate() throws IOException {
            System.out.print("beat");
        }
        // Oef 26
        public static void ex1()
        {
            try {
                new Organ().operate();
                new ThrowOtherException().operate();
            }
            catch(IOException e){
                System.out.println("IOException");
            }
            catch(RuntimeException e){
                System.out.println("Ja toch runtime");
            }
            finally {
                System.out.print("!");
            }
        }


        // Oef 31 checken
        // Oef 57 checken


}
