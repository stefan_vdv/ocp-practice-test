package be.optis.exception_exercises;
import java.util.Scanner;
public class Exercise2 {
    // Throws toevoegen zorgt ervoor dat er run method ook Exception moet declaren of
    // try-catch clause moet voorzien. Exception declaren kan altijd ook al throwt de
    // methode geen exception
        public void openDrawbridge() throws Exception {
//            try{
                //throw new Exception("Circle");
//            }
//            catch (Exception e){
//                System.out.println("Caught in method");
//            }

        }
        public static void run() /* throws Exception */{
            try (var e = new Scanner(System.in)) {
                new Exercise2().openDrawbridge();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
}
