package be.optis;

import be.optis.accessibility_exercises.InnerClassVariable;
import be.optis.exception_exercises.Exercise2;
import be.optis.exception_exercises.ThrowOtherException;

public class Main {

    public static void main(String[] args) {

        Exercise2.run();
    }
}
