package be.optis.accessibility_exercises;

interface CanSwim {
    void swim(final int distance);
}
public class FinalMethodParameter {
    // Oef 55, maar om 'final' in method parameter te testen
    /* final in swim method parameter zorgt er enkel voor dat distance
       niet nog eens opnieuw kan worden toegewezen in de method
    */
    final int distance = 2;

    public static void ex1() {
        final int distance = 3;
        CanSwim seaTurtle = new CanSwim() {
            @Override
            public void swim(final int distance) {
                //distance = 5;
                System.out.print(distance);
            }
        };
        //distance = 5;
        seaTurtle.swim(distance);
    }
}
