package be.optis.accessibility_exercises;

class Cinema {
    private String name = "Sequel";
    public Cinema(String name) {
        this.name = name;
    }
}
public class InnerClassVariable extends Cinema {

    // Oef 67, inner class variable accessibility test
    private String name = "adaptation";

    public InnerClassVariable(String movie) {
        super(movie);
        this.name = movie;
    }
    public static void ex1() {
        System.out.print(new InnerClassVariable("Trilogy").name);
    }

}
