package be.optis.static_exercises;

public class Enumerator {
    // Oef om te zien welk effect static op de enum heeft vraag 33
    enum Baby {EGG}

    static class Chick {
        enum Baby {EGG}

        enum Test {EGG}
    }

    public static void test() {
        boolean match = false;
        Chick.Baby egg = Chick.Baby.EGG;
        Chick.Test egg1 = Chick.Test.EGG;
        Baby egg2 = Baby.EGG;

        switch (egg) {
            case EGG:
                match = true;
        }

        System.out.println(match);
    }

}
