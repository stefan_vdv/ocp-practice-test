package be.optis.static_exercises;

public class Instantiation {
    // Vraag 46, static class instantiation
    public static class Ship {
        private enum Sail { // w1
            TALL {
                protected int getHeight() {
                    return
                            100;
                }
            },
            SHORT {
                protected int getHeight() {
                    return 2;
                }
            };

            protected abstract int getHeight();
        }

        public Sail getSail() {
            return Sail.TALL;
        }
    }

    public static void ex1() {
        var bottle = new Instantiation();
        Ship q = new Ship();
        System.out.print(q.getSail());
    }

}
